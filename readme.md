# REST API, based on Laravel framework

## Requirements
1. PHP 5.6+
2. Memcached 2.2+


### Set up process ###

1. Create new database
	CREATE database isoftbet DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
2. Create database user
	CREATE USER 'isoftbet_user'@'localhost' identified BY '38AuZ48H';
	GRANT ALL ON `isoftbet`.* TO 'isoftbet_user'@'localhost';
3. Find database dump file into database/migrantions and upload it to created DB;
4. Open app/config/database.php file and edit database credentials for mysql section;
5. For login to GUI use credentials: admin/06Vn25ks

### API specification ###

Api specification was created using Swagger
https://app.swaggerhub.com/api/krivoruchkos/iSoftBet_php_test/1.0.0

### ScreenShots ###

1. Login page http://prntscr.com/e9y3cn
2. View for list of transactions http://prntscr.com/e9xyuf
3. List of API methods http://prntscr.com/e9y0ye

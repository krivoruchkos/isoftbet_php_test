<?php

//general
Route::any('login', 'Authorization@login');
Route::get('logout', 'Authorization@logout');
Route::get('/', ['middleware' => 'auth', 'uses' => 'Dashboard@index']);
Route::group(['prefix' => 'api', 'middleware' => 'token-auth'], function () {
    Route::post('/customer', 'Customers@add');
    Route::post('/transaction', 'Transactions@add');
    Route::get('/transaction/{customerId}/{transactionId}', 'Transactions@get');
    Route::post('/transaction/getByFilter', 'Transactions@getByFilter');
    Route::delete('/transaction/{transactionId}', 'Transactions@delete');
    Route::patch('/transaction/{transactionId}', 'Transactions@update');
});


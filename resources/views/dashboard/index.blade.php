@extends('layouts/index')

@section('title', 'Transactions')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Filters</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="col-md-2">
                        <label for="amount">Amount</label>
                        <input type="text"  name="amount" id="amount" class="form-control" >
                    </div>
                    <div class="col-md-2">
                        <label for="date">Date</label>
                        <input type="text"  name="date" id="date" class="form-control date-picker" >
                    </div>
                    <div class="col-md-2">
                        <label for="customer_id">Customer ID</label>
                        <input type="text" id="customer_id" class="form-control" name="customer_id">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>List</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content search-hide">
                    <table id="datatable" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Customer</th>
                                <th>Amount</th>
                                <th>Date</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
<script type="text/javascript">
    var table = $('#datatable').DataTable({
        columns: [
            {data: "transactionId", orderable: false},
            {data: "customerId", orderable: false},
            {data: "amount", orderable: false},
            {data: "date", orderable: false}
        ],
        processing: true,
        serverSide: true,
        lengthMenu: [[5, 10, 50], [5, 10, 50]],
        ajax: {
            url: 'api/transaction/getByFilter',
            type: 'POST',
            'headers': {
                'Authorization': '{{ Auth::user()->api_token }}'
            },
            data: function(d) {
                console.log(d);
                var data = {
                        offset: d.start,
                        limit: d.length
                    };
                if (d.columns[3].search.value) {
                    data.date = d.columns[3].search.value;
                }
                if (d.columns[2].search.value) {
                    data.amount = d.columns[2].search.value;
                }
                if (d.columns[1].search.value) {
                    data.customerId = d.columns[1].search.value;
                }
                d = JSON.stringify(data);
                return d;
            }
        }
    });
    $(document).ready(function() {
        $('.date-picker').daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            autoUpdateInput: false,
            locale: {
                format: 'YYYY-MM-DD'
            }
        }, function (chosen_date) {
            $(this.element).val(chosen_date.format('YYYY-MM-DD')).trigger('change');
        });
    });

    function search(el, column, event) {
        $(el).on(event, function (e) {
            table.column(column).search(this.value).draw();
/*            if (this.value == "")
                table.column(column).search("").draw();*/

        });
    }

    search('#amount', 2, 'keyup');
    search('#date', 3, 'change');
    search('#customer_id', 1, 'keyup');
</script>
@endsection
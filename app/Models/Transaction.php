<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class Transaction extends Model {

    protected $table = 'transactions';
    protected $fillable = ['customer_id', 'amount'];

    public function scopeCustomerId($query, $customerId)
    {
        if ($customerId) {
            return $query->where('customer_id', 'like', '%' . $customerId . '%');
        }

        return $query;
    }

    public function scopeAmount($query, $amount)
    {
        if ($amount) {
            return $query->where('amount', 'like', '%' . $amount . '%');
        }

        return $query;
    }

    public function scopeDate($query, $date)
    {
        if ($date) {
            return $query->where('created_at', 'like', '%' . $date . '%');
        }

        return $query;
    }


}
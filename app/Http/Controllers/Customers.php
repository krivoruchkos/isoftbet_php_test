<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Validator;
use App\Library\Logger;
use App\Models\Customer;
class Customers extends BaseController
{
    public function add()
    {
        $request = request()->json()->all();
        $validator = Validator::make($request, [
            'name' => 'required|min:2',
            'cnp' => 'required|bool',
        ]);

        if ($validator->fails()) {
            $errors = $validator->messages()->all();
            $response = [
                'success' => false,
                'errors' => $errors
            ];
            Logger::add(request(), $response);

            return response()->json($response, 400);
        }

        $customer = Customer::create([
            'name' => $request['name'],
            'cnp' => $request['cnp']
        ]);

        $response = [
            'success' => true,
            'customerId' => $customer->id
        ];
        Logger::add(request(), $response);

        return response()->json($response, 201);
    }

}

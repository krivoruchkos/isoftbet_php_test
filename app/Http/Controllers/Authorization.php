<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class Authorization extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function login(Request $request)
    {
        if ($request->isMethod('post')) {
            $this->validate($request, [
                'username' => 'required|min:2',
                'password' => 'required'
            ]);

            if ($r = Auth::attempt(['username' => $request->get('username'), 'password' => $request->get('password')])) {
                return redirect()->intended('/');
            }
        }

        return view('auth/login');
    }

    public function logout()
    {
        Auth::logout();

        return redirect('login');
    }
}

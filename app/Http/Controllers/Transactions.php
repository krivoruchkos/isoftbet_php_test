<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Cache;
use App\Models\Transaction;
use App\Library\Logger;
class Transactions extends BaseController
{
    public function add()
    {
        $request = request()->json()->all();
        $validator = Validator::make($request, [
            'customerId' => 'required|integer|exists:customers,id',
            'amount' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            $errors = $validator->messages()->all();
            $response = [
                'success' => false,
                'errors' => $errors
            ];
            Logger::add(request(), $response);
            return response()->json($response, 400);
        }

        $transaction = Transaction::create([
            'customer_id' => $request['customerId'],
            'amount' => $request['amount']
        ]);

        $response = [
            'success' => true,
            'transactionId' => $transaction->id,
            'customerId' => $transaction->customer_id,
            'amount' => $transaction->amount,
            'date' => $transaction->created_at->format('Y-m-d H:i:s')
        ];
        Logger::add(request(), $response);

        return response()->json($response, 201);

    }

    public function get($customerId, $transactionId)
    {

        $validator = Validator::make(['customerId' => $customerId, 'transactionId' => $transactionId], [
            'customerId' => 'required|integer',
            'transactionId' => 'required|integer',
        ]);

        if ($validator->fails()) {
            $errors = $validator->messages()->all();
            $response = [
                'success' => false,
                'errors' => $errors
            ];
            Logger::add(request(), $response);
            return response()->json($response, 400);
        }

        $cacheKey = "transaction_get_" . $transactionId . "_" . $customerId;

        if (Cache::has($cacheKey)) {
            $response = Cache::get($cacheKey);
        }
        else {
            $transaction = Transaction::where(['id' => $transactionId, 'customer_id' => $customerId])->first();
            if ($transaction) {
                $response = [
                    'success' => true,
                    'transactionId' => $transaction->id,
                    'amount' => $transaction->amount,
                    'date' => $transaction->created_at->format('Y-m-d H:i:s')
                ];
            }
            else {
                $response = [
                    'success' => false
                ];
            }
            Cache::put($cacheKey, $response, 60);
        }
        Logger::add(request(), $response);

        return response()->json($response);
    }

    public function getByFilter()
    {
        $request = request()->json()->all();

        $validator = Validator::make($request, [
            'customerId' => 'integer',
            'amount' => 'numeric',
            'date' => 'date_format:"Y-m-d"',
        ]);

        if ($validator->fails()) {
            $response = [
                'success' => true,
                'recordsTotal' => 0,
                'recordsFiltered' => 0,
                'data' => [],
            ];
            Logger::add(request(), $response);
            return response()->json($response);
        }

        $customerId = isset($request['customerId']) ? $request['customerId'] : '';
        $amount = isset($request['amount']) ? $request['amount'] : '';
        $date = isset($request['date']) ? $request['date'] : '';
        $offset = isset($request['offset']) ? (int)$request['offset'] : 5;
        $limit = isset($request['limit']) ? (int)$request['limit'] : 10;

        $cacheKey = "transaction_filter_" . $customerId . "_" . $amount . "_" . $date . "_" . $offset . "_" . $limit;

        if (Cache::has($cacheKey)) {
            $response = Cache::get($cacheKey);
        }
        else {
            $transaction = Transaction::take($limit);

            if ($customerId) {
                $transaction->customerId($request['customerId']);
            }

            if ($amount) {
                $transaction->amount($request['amount']);
            }

            if ($date) {
                $transaction->date($request['date']);
            }

            $recordsFiltered = $transaction->count();
            $transaction->skip($offset);
            $transactions = $transaction->get();

            $data = [];

            foreach ($transactions as $transaction) {
                $data[] = [
                    'transactionId' => $transaction->id,
                    'customerId' => $transaction->customer_id,
                    'amount' => $transaction->amount,
                    'date' => $transaction->created_at->format('Y-m-d H:i:s')
                ];
            }

            $response = [
                'success' => true,
                'recordsTotal' => Transaction::count(),
                'recordsFiltered' => $recordsFiltered,
                'data' => $data,
            ];

            Cache::put($cacheKey, $response, 60);
        }
        Logger::add(request(), $response);

        return response()->json($response);
    }

    public function delete($transactionId)
    {
        if (Transaction::where('id', (int)$transactionId)->delete()) {
            $response = ['success' => true];
        }
        else {
            $response = ['success' => false];
        }
        Logger::add(request(), $response);

        return response()->json($response);
    }

    public function update($transactionId)
    {
        $request = request()->json()->all();
        $validator = Validator::make($request, [
            'amount' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            $errors = $validator->messages()->all();
            $response = [
                'success' => false,
                'errors' => $errors
            ];
            Logger::add(request(), $response);
            return response()->json($response, 400);
        }

        Transaction::where('id', (int)$transactionId)->update(['amount' => $request['amount']]);
        $transaction = Transaction::find((int)$transactionId);
        $response =  [
            'success' => true,
            'transactionId' => $transaction->id,
            'customerId' => $transaction->customer_id,
            'amount' => $transaction->amount,
            'date' => $transaction->created_at->format('Y-m-d H:i:s')
        ];
        Logger::add(request(), $response);

        return response()->json($response);
    }
}

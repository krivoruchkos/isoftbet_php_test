<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\Admin;

class TokenAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = $request->header('Authorization');

        if (!$admin = Admin::where('api_token', $token)->first()) {
            return response()->json(['success'=>false, 'error' => 'Unauthorized'], 401, ['X-Header-One' => 'Header Value']);
        }
        $request->merge(['adminId' => $admin->id]);

        return $next($request);
    }
}

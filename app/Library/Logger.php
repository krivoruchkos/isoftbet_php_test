<?php

namespace App\Library;

use App\Models\Log;
class Logger
{
    public static $instance = null;

    private function __construct()
    {

    }

    public static function init()
    {
        if (!self::$instance)
            self::$instance = new self;

        return self::$instance;
    }

    public static function add($request, $response)
    {

        $data = [
            'admin_id' => $request->adminId,
            'uri' => $request->path(),
            'request' => json_encode($request->json()->all()),
            'response' => json_encode($response)
        ];

        Log::create($data);
    }
}